// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interacteble.h"
#include "SnakeElementBase.generated.h"

class ASnakeBase;
class UStaticMeshComponent;

UCLASS()
class SNAKE_API ASnakeElementBase : public AActor, public IInteracteble
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeElementBase();

	UPROPERTY(VisibleAnywhere,BlueprintReadOnly)
	UStaticMeshComponent* MeshComponent;

	UPROPERTY()
	ASnakeBase* SnakeOwner;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UFUNCTION(BlueprintNativeEvent)
	void SetFirstElementType();
	void SetFirstElementType_Implementation();
	virtual void Interact(AActor* Interactor, bool isHead) override;

	UFUNCTION(BlueprintNativeEvent)
	void HeadHitTail();
	void HeadHitTail_Implementation();

	UFUNCTION()
		void HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
			UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep,
			const FHitResult& SweepResult);

	UFUNCTION()
		void ToggleCollision();
};


