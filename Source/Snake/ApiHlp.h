#pragma once
#define CHECK_VALID(X) \
if (!IsValid(X)) \
return;
#include <ctime>

inline int random(int min, int max)
{
    srand(time(nullptr) + rand());

    int temp = max - min;

    if (temp == 0)
        temp = 1;

    const int value = rand() % temp + min;
    return value;
}
