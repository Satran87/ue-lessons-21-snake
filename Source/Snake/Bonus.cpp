// Fill out your copyright notice in the Description page of Project Settings.


#include "Bonus.h"

#include "SnakeBase.h"
#include "ApiHlp.h"

// Sets default values
ABonus::ABonus()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	currentBonus = BonusType::SpeedUp;

}

// Called when the game starts or when spawned
void ABonus::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABonus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ABonus::Interact(AActor* Interactor, bool isHead)
{
	CHECK_VALID(Interactor)
	ASnakeBase* SnakeBase = Cast<ASnakeBase>(Interactor);
	CHECK_VALID(SnakeBase)
	if (!isHead)
		return;
	BonusHit();
	switch (currentBonus)
	{
	case BonusType::SpeedUp:
		{
		SnakeBase->SetSpeed(-0.1f);
		this->Destroy();
		break;
		} 
	case BonusType::SpeedDown:
	{
		SnakeBase->SetSpeed(0.1f);
		this->Destroy();
		break;
	}
	case BonusType::CutTail:
	{
		SnakeBase->CutTail();
		this->Destroy();
		break;
	}
	default: ;
	}
}
