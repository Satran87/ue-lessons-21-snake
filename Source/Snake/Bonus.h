// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interacteble.h"
#include "Bonus.generated.h"

UENUM(BlueprintType)
enum class BonusType : uint8
{
	SpeedUp UMETA(DisplayName = "SpeedUp"),
	SpeedDown UMETA(DisplayName = "SpeedDown"),
	CutTail UMETA(DisplayName = "CutTail")
};

UCLASS()
class SNAKE_API ABonus : public AActor, public IInteracteble
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABonus();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	UPROPERTY(EditAnywhere,BlueprintReadWrite, Category=Enum)
	BonusType currentBonus;
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void Interact(AActor* Interactor, bool isHead) override;
	UFUNCTION(BlueprintImplementableEvent)
	void BonusHit();

};
