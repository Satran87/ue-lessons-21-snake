// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Engine/Engine.h"
#include "Interacteble.h"
#include "ApiHlp.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 10.f;
	LastMoveDirection = EMovementDirection::DOWN;
}


void ASnakeBase::SetSpeed(float speed)
{
	if(GetSpeed()<0.1f || GetSpeed() > 3.0f)
		return;
	MovementSpeed += speed;
	SetActorTickInterval(MovementSpeed);
}

float ASnakeBase::GetSpeed()
{
	return MovementSpeed;
}

void ASnakeBase::CutTail()
{
	const auto baseSize = SnakeElemnts.Num();
	if ((baseSize - 1) < 1)
		return;
	SnakeElemnts.Last()->Destroy();
	const auto lastElement = SnakeElemnts.IndexOfByKey(SnakeElemnts.Last());
	SnakeElemnts.RemoveAt(lastElement);
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(4);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement(int32 ElementsNum)
{
	for(int t=0;t<ElementsNum;++t)
	{
		auto num = SnakeElemnts.Num();
		auto xLocation = ElementSize*num;
		const FVector NewLocation(xLocation ,0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElement->SnakeOwner = this;
		int32 ElementIndex=SnakeElemnts.Add(NewSnakeElement);
		if(ElementIndex==0)
		{
			NewSnakeElement->SetFirstElementType();
		}
		else
			NewSnakeElement->SetActorHiddenInGame(true);
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);
	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		{
			MovementVector.X += ElementSize;
			break;
		};
	case EMovementDirection::DOWN:
		{
			MovementVector.X -= ElementSize;
			break;
		};
	case EMovementDirection::LEFT:
		{
			MovementVector.Y += ElementSize;
			break;
		};
	case EMovementDirection::RIGHT:
		{
			MovementVector.Y -= ElementSize;
			break;
		};
	default: ;
	}
	SnakeElemnts[0]->ToggleCollision();
	for(int t=SnakeElemnts.Num()-1;t>0;t--)
	{
		
		auto CurrentElement = SnakeElemnts[t];
		auto PrevElement = SnakeElemnts[t - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
		CurrentElement->SetActorHiddenInGame(false);
	}
	SnakeElemnts[0]->AddActorWorldOffset(MovementVector);
	SnakeElemnts[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	CHECK_VALID(OverlappedElement)
	CHECK_VALID(Other)
	int32 ElemIndex;
	SnakeElemnts.Find(OverlappedElement, ElemIndex);
	bool isFirst = ElemIndex == 0;
	IInteracteble* InteractebleInterface = Cast<IInteracteble>(Other);//NOTE:������ �������� ������ ���� ��� ������� � BP � ������� ����������
	if(InteractebleInterface!=nullptr)
	{
		InteractebleInterface->Interact(this, isFirst);
	}
}

